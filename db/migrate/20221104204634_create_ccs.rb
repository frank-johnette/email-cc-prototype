class CreateCcs < ActiveRecord::Migration[6.0]
  def change
    create_table :ccs do |t|
      t.string :email

      t.timestamps
    end
  end
end
