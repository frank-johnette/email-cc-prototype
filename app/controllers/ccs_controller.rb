class CcsController < ApplicationController
  before_action :set_cc, only: %i[ show edit update destroy ]

  # GET /ccs or /ccs.json
  def index
    @ccs = Cc.all
  end

  # GET /ccs/1 or /ccs/1.json
  def show
  end

  # GET /ccs/new
  def new
    @cc = Cc.new
  end

  # GET /ccs/1/edit
  def edit
  end

  # POST /ccs or /ccs.json
  def create
    @cc = Cc.new(cc_params)

    respond_to do |format|
      if @cc.save
        format.turbo_stream do
          render turbo_stream: turbo_stream.update("cc_list", partial: "ccs/cc_list", locals: {css: @css})
        end
        format.html { redirect_to ccs_path, notice: "Cc was successfully created." }
        format.json { render :show, status: :created, location: @cc }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @cc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ccs/1 or /ccs/1.json
  def update
    respond_to do |format|
      if @cc.update(cc_params)
        format.html { redirect_to cc_url(@cc), notice: "Cc was successfully updated." }
        format.json { render :show, status: :ok, location: @cc }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @cc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ccs/1 or /ccs/1.json
  def destroy
    @cc.destroy

    respond_to do |format|
      format.turbo_stream do
        render turbo_stream: turbo_stream.update("cc_list", partial: "ccs/cc_list", locals: {css: @css})
      end
      format.html { redirect_to ccs_path }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cc
      @cc = Cc.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cc_params
      params.require(:cc).permit(:email)
    end
end
