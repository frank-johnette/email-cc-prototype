json.extract! cc, :id, :email, :created_at, :updated_at
json.url cc_url(cc, format: :json)
