import { Controller } from  "@hotwired/turbo-rails"      // "stimulus"

export default class extends Controller {
    reset() {
        this.element.reset()
    }
}